Name:          ambience-yrgb-abstract-light
Version:       0.2.0
Release:       1
Summary:       Four different colored light ambiences
Group:         System/Tools
Vendor:        eson
Distribution:  SailfishOS
Packager:      eson <eson@svenskasprakfiler.se>
URL:           https://svenskasprakfiler.se

License:       GPL

%description
Four different colored light ambiences based on abstract background images.

%files
%defattr(-,root,root,-)
/usr/share/ambience/*

%post
chmod 755 /usr/share/ambience/{name}
chmod 755 /usr/share/ambience/{name}/images
chmod 755 /usr/share/ambience/{name}/sounds
chmod 644 /usr/share/ambience/{name}/*.*
chmod 644 /usr/share/ambience/{name}/images/*.*
chmod 644 /usr/share/ambience/{name}/sounds/*.*
systemctl-user restart ambienced.service

%postun
if [ $1 = 0 ]; then
rm -rf /usr/share/ambience/{name}
systemctl-user restart ambienced.service
else
if [ $1 = 1 ]; then
echo "Upgrading"
systemctl-user restart ambienced.service
fi
fi

%changelog
* Fri Nov 09 2018 0.2
- Changed highlight colors for slightly better visibility.

* Sun Nov 04 2018 0.1
- Initial release.

